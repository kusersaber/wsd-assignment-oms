package uts.wsd;

import java.io.*;
import java.util.*;
import javax.xml.bind.annotation.*;
/**
 *
 * @author tanzeyu
 */
@XmlRootElement(name = "movies")
@XmlAccessorType(XmlAccessType.FIELD)
public class Movies implements Serializable{
    @XmlElement(name = "movie")
    private ArrayList<Movie> movies = new ArrayList<>();

    public Movies() {
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }
    
    public void addMovie(Movie movie){
        movies.add(movie);
    }
    public void remove(Movie movie){
        movies.remove(movie);
    }
    public void addAll(ArrayList<Movie> list){
        this.movies.addAll(list);
    }
    public void removeAll(ArrayList<Movie> list){
        this.movies.removeAll(list);
    }
    /*public ArrayList<Movie> getMatches(int id, String genre, String title){
        ArrayList<Movie> matches = new ArrayList<>();
        for(Movie movie:movies)
            if(movie.matchAny(id,genre,title))
                matches.add(movie);
        return matches;
    }*/
    
    public ArrayList<Movie> getMatches(String input){
        ArrayList<Movie> matches = new ArrayList<>();
        for(Movie movie:movies)
            if(movie.matchTitle(input)) {
                matches.add(movie);
            } else if (movie.matchGenre(input)) {
                matches.add(movie);
            }
        return matches;
    }
    
    public ArrayList<Movie> getYearBetween(String input1, String input2){
        ArrayList<Movie> biggerEqualsMatches = new ArrayList<>();
        ArrayList<Movie> smallerEqualsMatches = new ArrayList<>();
        ArrayList<Movie> duplicateMatches = new ArrayList<>();
        
        for(Movie movie:movies)
            if(movie.isBiggerThanAndEquals(input1)) {
                biggerEqualsMatches.add(movie);
            }
        
        for(Movie movie:movies)
            if(movie.isSmallerThanAndEquals(input2)) {
                smallerEqualsMatches.add(movie);
            }
        
        for (Movie bigger : biggerEqualsMatches) {
            
            for (Movie smaller : smallerEqualsMatches) {
                
                if ((bigger.getId() == smaller.getId())) {
                    
                    duplicateMatches.add(bigger);
                }
            }
        }
        return duplicateMatches;
    }
        
    public Movie getMovie(int id) {
        for (Movie movie : movies) {
            if (movie.matchId(id)) {
                return movie;
            }
        }
        return null;
    }
}
