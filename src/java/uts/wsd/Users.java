package uts.wsd;

import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "users")
public class Users implements Serializable {
    
    @XmlElement(name = "user")
    private ArrayList<User> list = new ArrayList<>();

    public ArrayList<User> getList() {
        return list;
    }

    public void addUser(User user) {
        list.add(user);
    }

    public User getUser(String email) {
        for (User user : list) {
            if (user.matchEmail(email)) {
                return user;
            }
        }
        return null;
    }

    public User login(String email, String password) {        
        for (User user : list) {
            if (user.matchEmail(email) && user.matchPassword(password)) {
                return user; 
            }
        }
        return null; 
    }

    public ArrayList<User> getRemoveList(String email){
        ArrayList<User> removelist = new ArrayList<>();
        for(User user:list)
            if(user.matchEmail(email))
                removelist.add(user);
        return removelist;
    }
    
    public void remove(User user) {
        list.removeAll(getRemoveList(user.getEmail()));
    }
    
    public void updateList(User user){
        remove(user);
        addUser(user);
    }
}
