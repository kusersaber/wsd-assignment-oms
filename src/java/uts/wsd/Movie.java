package uts.wsd;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
/**
 *
 * @author tanzeyu
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "movie")
public class Movie implements Serializable {

    @XmlElement(name = "id")
    private int id;
    @XmlElement(name = "title")
    private String title;
    @XmlElement(name = "genre")
    private String genre;
    @XmlElement(name = "releaseDate")
    private String releaseDate;
    @XmlElement(name = "price")
    private double price;   
    @XmlElement(name = "availableCopies")
    private int availableCopies;

    public Movie() {
    }
    
    public Movie(int id, String title,  String genre, String releaseDate, double price, int availableCopies) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.availableCopies = availableCopies;
    }

    public void updateMovie(String title, String genre, String releaseDate, double price, int availableCopies) {        
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;       
        this.price = price;
        this.availableCopies = availableCopies;
    }

    public int getId() {
        return id;
    }    

    public String getTitle() {
        return title;
    }
    
    public String getGenre() {
        return genre;
    }

    public String getReleaseDate() {
        return releaseDate;
    }
    
    public int getReleaseYear() {
        return Integer.parseInt(releaseDate.substring(0, 4));
    }
    
    public double getPrice() {
        return price;
    }

    public int getAvailableCopies() {
        return availableCopies;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }

    public void setAvailableCopies(int availableCopies) {
        this.availableCopies = availableCopies;
    }

    public boolean matchId(int id) {
        return (this.id == id);
    }

    public boolean matchGenre(String genre) {
        return this.genre.equalsIgnoreCase(genre.toLowerCase().trim());
    }

    public boolean matchTitle(String title) {
        return this.title.equalsIgnoreCase(title.toLowerCase().trim());
    }

    public boolean matchAny(int id, String genre, String title) {
        return matchGenre(genre) || matchTitle(title) || matchId(id);
    }
    
    public boolean isBiggerThanAndEquals(String input) {
        int inputYear = Integer.parseInt(input.substring(0, 4));
        int year = Integer.parseInt(releaseDate.substring(0, 4));
        return inputYear <= year;
    }
    
    public boolean isSmallerThanAndEquals(String input) {
        int inputYear = Integer.parseInt(input.substring(0, 4));
        int year = Integer.parseInt(releaseDate.substring(0, 4));
        return year <= inputYear;
    }
}
