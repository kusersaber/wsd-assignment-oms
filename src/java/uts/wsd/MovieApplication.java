package uts.wsd;

import java.io.*;
import javax.xml.bind.*;

/**
 *
 * @author tanzeyu
 */
public class MovieApplication implements Serializable {

    private String filePath;
    private Movies movies;

    public MovieApplication() {
    }

    public MovieApplication(String filePath, Movies movies) {
        this.filePath = filePath;
        this.movies = movies;
    }

    public String getFilePath() {
        return filePath;
    }

    public Movies getMovies() {
        return movies;
    }

    public void setMovies(Movies movies) {
        this.movies = movies;
    }

    public void setFilePath(String filePath) throws JAXBException, FileNotFoundException, IOException {
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        Unmarshaller u = jc.createUnmarshaller();
        this.filePath = filePath;
        FileInputStream fin = new FileInputStream(filePath);
        movies = (Movies) u.unmarshal(fin);
        fin.close();
    }

    public void updateXML(Movies movies, String filePath) throws Exception {
        this.movies = movies;
        this.filePath = filePath;
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        FileOutputStream fout = new FileOutputStream(filePath);
        m.marshal(movies, fout);
        fout.close();
    }

    public void saveMovies() throws JAXBException, IOException {
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        FileOutputStream fout = new FileOutputStream(filePath);
        m.marshal(movies, fout);
        fout.close();
    }

    public void editMovie(Movie movie, double price, String title, String releaseDate, String genre, int availableCopies) throws Exception {
        movies.remove(movie);
        movie.updateMovie(title, genre, releaseDate, price, availableCopies);
        movies.addMovie(movie);
        updateXML(movies, filePath);
    }
}
