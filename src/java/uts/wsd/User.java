package uts.wsd;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "user")
public class User implements Serializable {
    
    @XmlElement(name = "email")
    private String email;
    @XmlElement(name = "password")
    private String password;
    @XmlElement(name = "fullName")
    private String fullName;
    @XmlElement(name = "dob")
    private String dob;
    @XmlElement(name = "phone")
    private String phone;
    @XmlElement(name = "address")
    private String address;

    public User() {
    }

    public User(String email, String password, String fullName, String dob, String phone, String address) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.dob = dob;
        this.phone = phone;
        this.address = address;
    }
    
    public void updateDetails(String password, String fullName, String dob, String phone, String address){
        this.password = password;
        this.fullName = fullName;
        this.dob = dob;
        this.phone = phone;
        this.address = address;  
    }

    public boolean matchEmail(String email){
        return this.email.equals(email.trim());
    }
    
    public boolean matchPassword(String password){
        return this.password.equals(password.trim());
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDob() {
        return dob;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    } 
}
