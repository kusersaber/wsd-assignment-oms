package uts.wsd;

import java.io.*;
import java.util.*;
import javax.xml.bind.annotation.*;

/**
 *
 * @author MarkLee
 */
@XmlRootElement(name = "orders")
@XmlAccessorType(XmlAccessType.FIELD)
public class Orders implements Serializable {

    @XmlElement(name = "order")
    private ArrayList<Order> list = new ArrayList<>();

    public Orders() {
    }

    public ArrayList<Order> getList() {
        return list;
    }
    
    public void setList(ArrayList<Order> orders) {
        this.list = orders;
    }

    public void addOrder(Order order) {
        list.add(order);
    }
    
    public void clear() {
        list.clear();
    }

    public void remove(Order order) {
        list.remove(order);
    }

    public ArrayList<Order> getRemoveList(int id) {
        ArrayList<Order> removelist = new ArrayList<>();
        for (Order order : list) {
            if (order.getId() == id) {
                removelist.add(order);
            }
        }
        return removelist;
    }
    
    public void removeAll(ArrayList<Order> orders) {       
        list.removeAll(orders);
    }

    public void removeAll(Order order) {
        list.removeAll(getRemoveList(order.getId()));
    }

    public void updateList(Order order) {
        remove(order);
        addOrder(order);
    }
    
    public Order getOrder(int id) {
        for (Order order : list) {
            if (order.matchId(id)) {
                return order;
            }
        }
        return null;
    }
}