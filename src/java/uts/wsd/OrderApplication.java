package uts.wsd;

import java.io.*;
import javax.xml.bind.*;
/**
 *
 * @author MarkLee
 */
public class OrderApplication  implements Serializable {
    
    private String filePath;
    private Orders orders;

    public OrderApplication() {
    }

    public OrderApplication(String filePath, Orders orders) {
        super();
        this.filePath = filePath;
        this.orders = orders;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) throws Exception {
       
        JAXBContext jc = JAXBContext.newInstance(Orders.class);
        Unmarshaller u = jc.createUnmarshaller();
        this.filePath = filePath;        
        FileInputStream fin = new FileInputStream(filePath);
        orders = (Orders) u.unmarshal(fin); 	
        fin.close();
    }

    public void updateXML(Orders orders, String filePath) throws Exception {
        this.orders = orders;
        this.filePath = filePath;
        JAXBContext jc = JAXBContext.newInstance(Orders.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        FileOutputStream fout = new FileOutputStream(filePath);
        m.marshal(orders, fout);
        fout.close();
    }
   
    public void saveOrders() throws JAXBException, IOException {
        JAXBContext jc = JAXBContext.newInstance(Orders.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        FileOutputStream fout = new FileOutputStream(filePath);
        m.marshal(orders, fout);
        fout.close();
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }
    
    public void editO(Order order, int movieId) throws Exception{
        orders.remove(order);
        order.setMovieId(movieId);
        orders.addOrder(order);
        updateXML(orders,filePath);
    }
}