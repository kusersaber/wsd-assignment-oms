package uts.wsd;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 *
 * @author MarkLee
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "order")
public class Order implements Serializable {

    @XmlElement(name = "id")
    private int id;
    @XmlElement(name = "movieId")
    private int movieId;
    @XmlElement(name = "movieTitle")
    private String movieTitle;
    @XmlElement(name = "movieGenre")
    private String movieGenre;
    @XmlElement(name = "moviePrice")
    private double moviePrice;
    @XmlElement(name = "movieReleaseDate")
    private String movieReleaseDate;
    @XmlElement(name = "movieCopiesPurchased")
    private int movieCopiesPurchased;
    @XmlElement(name = "userFullName")
    private String userFullName;
    @XmlElement(name = "userEmail")
    private String userEmail;
    @XmlElement(name = "payMethod")
    private String payMethod;
    @XmlElement(name = "totalSale")
    private double totalSale;
    @XmlElement(name = "status")
    private String status;

    public Order() {
    }

    public Order(int id, int movieId, String movieTitle, 
            String movieGenre, double moviePrice, String movieReleaseDate, 
            int movieCopiesPurchased, String userFullName, String userEmail, 
            String payMethod, double totalSale, String status) {
        
        this.id = id;
        this.movieId = movieId;
        this.movieTitle = movieTitle;
        this.movieGenre = movieGenre;
        this.moviePrice = moviePrice;
        this.movieReleaseDate = movieReleaseDate;
        this.movieCopiesPurchased = movieCopiesPurchased;
        this.userFullName = userFullName;
        this.userEmail = userEmail;
        this.payMethod = payMethod;
        this.totalSale = totalSale;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public String getMovieGenre() {
        return movieGenre;
    }

    public double getMoviePrice() {
        return moviePrice;
    }

    public String getMovieReleaseDate() {
        return movieReleaseDate;
    }

    public int getMovieCopiesPurchased() {
        return movieCopiesPurchased;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public double getTotalSale() {
        return totalSale;
    }

    public String getStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }
    
    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    public void setMoviePrice(double moviePrice) {
        this.moviePrice = moviePrice;
    }

    public void setMovieReleaseDate(String movieReleaseDate) {
        this.movieReleaseDate = movieReleaseDate;
    }

    public void setMovieCopiesPurchased(int movieCopiesPurchased) {
        this.movieCopiesPurchased = movieCopiesPurchased;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public void setTotalSale(double totalSale) {
        this.totalSale = totalSale;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public boolean matchId(int id) {
        return (this.id == id);
    }
}