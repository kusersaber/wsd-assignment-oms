package uts.controller;

import java.io.*;
import javax.xml.bind.*;
import uts.wsd.*;
import uts.wsd.*;

public class TestJAXB implements Serializable {

    public static void main(String[] args) throws Exception {
        Users users = new Users();
        users.addUser(new User("rick@gmail.com", "rickb", "Rick White", "1996-02-01", "0430-622-111", "City"));
        users.addUser(new User("jack@hotmail.com", "jackr", "Jack Reacher", "1995-08-07", "0430-623-151", "City"));
        users.addUser(new User("smith@gmail.com", "lsmith", "Laura Smith", "1996-05-13", "0430-623-151", "City"));

        Movies movies = new Movies();
        movies.addMovie(new Movie(12052548, "E.T.", "Sci-Fi", "1984-11-20", 50.0, 100));
        movies.addMovie(new Movie(12047469, "The Matrix", "Action", "1981-10-12", 50.0, 100));
        movies.addMovie(new Movie(10258456, "Halloween", "Horror ", "1985-09-20", 50.0, 100));
        //code to convert objects to XML...
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);        
        m.marshal(movies, System.out);
    }
}
