package uts.controller;

import java.io.*;
import javax.xml.bind.*;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import uts.wsd.*;
 
public class TestXMLFactory {
    
    public static void main(String[] args) throws Exception {
        // XML Data      
        MovieApplication movieApp = new MovieApplication();
        String filePath = (new File("\\C:\\Users\\Louis\\Desktop\\new_folder\\wsd_assg\\build\\web\\WEB-INF\\movies.xml")).getAbsolutePath();
        movieApp.setFilePath(filePath);     
        Movies movies = movieApp.getMovies();
        
        // Create Transformer
        TransformerFactory tf = TransformerFactory.newInstance();
        StreamSource xslt = new StreamSource("web/xsl/movies.xsl");
        Transformer transformer = tf.newTransformer(xslt);
        
        // Source
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        JAXBSource source = new JAXBSource(jc, movies); 
        // Result
        StreamResult result = new StreamResult(System.out);  
        
        // Transform
        transformer.transform(source, result);
        
        
        
        // XML Data      
        OrderApplication orderApp = new OrderApplication();
        String filePath2 = (new File("\\C:\\Users\\Louis\\Desktop\\new_folder\\wsd_assg\\build\\web\\WEB-INF\\history.xml")).getAbsolutePath();
        movieApp.setFilePath(filePath2);     
        Orders orders = orderApp.getOrders();
        
        // Create Transformer
        TransformerFactory tf2 = TransformerFactory.newInstance();
        StreamSource xslt2 = new StreamSource("web/xsl/history.xsl");
        Transformer transformer2 = tf2.newTransformer(xslt2);
        
        // Source
        JAXBContext jc2 = JAXBContext.newInstance(Orders.class);
        JAXBSource source2 = new JAXBSource(jc2, orders); 
        // Result
        StreamResult result2 = new StreamResult(System.out);  
        
        // Transform
        transformer.transform(source2, result2);
    } 
}
