package uts.controller;

import java.io.Serializable;
import java.io.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import uts.wsd.*;

public class XMLGenerator implements Serializable{

    public XMLGenerator() {
        super();
    }    
    
    public void renderHTML(Movies movies, Writer out) throws TransformerConfigurationException, JAXBException, TransformerException{
        // Create Transformer
        TransformerFactory tf = TransformerFactory.newInstance();
        StreamSource xslt = new StreamSource("\\C:\\Users\\Louis\\Desktop\\new_folder\\wsd_assg\\build\\web\\xsl\\movies.xsl");
        Transformer transformer = tf.newTransformer(xslt); 
        // Source
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        JAXBSource source = new JAXBSource(jc, movies); 
        // Result
        StreamResult result = new StreamResult(out);         
        // Transform
        transformer.transform(source, result);
    }
    
    public void renderHTML(Orders orders, Writer out) throws TransformerConfigurationException, JAXBException, TransformerException{
        // Create Transformer
        TransformerFactory tf = TransformerFactory.newInstance();
        StreamSource xslt = new StreamSource("\\C:\\Users\\Louis\\Desktop\\new_folder\\wsd_assg\\build\\web\\xsl\\history.xsl");
        Transformer transformer = tf.newTransformer(xslt); 
        // Source
        JAXBContext jc = JAXBContext.newInstance(Movies.class);
        JAXBSource source = new JAXBSource(jc, orders); 
        // Result
        StreamResult result = new StreamResult(out);         
        // Transform
        transformer.transform(source, result);
    }
}

