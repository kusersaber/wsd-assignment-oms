<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Register Page</title>
    </head>
    <body>
        <h1>Register</h1>
        <br>
        <form action="welcomeLoginRegister.jsp" method="post">
            <table>
                <tr><td>Email:</td><td><input type="text" name="email"></td></tr>
                <tr><td>Password:</td><td><input type="password" name="password"></td></tr>
                <tr><td>Full Name:</td><td><input type="text" name="fullName"></td></tr>
                <tr><td>Date of Birth:</td><td><input type="text" name="dob"></td></tr>
                <tr><td>Phone:</td><td> <input type="text" name ="phone"></td></tr>
                <tr><td>Address</td><td><input type="text" name="address"></td></tr>
                <tr><td></td>
                    <td>
                        <input type="submit" value="Register">
                        <br>
                        <br>
                        You have already an account? >>> <a href="login.jsp">Login</a>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
