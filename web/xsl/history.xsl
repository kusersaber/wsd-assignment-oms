<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>      
            <head>
                <!-- <link rel="stylesheet" href="css/style.css" type="text/css"></link> -->
            </head>     
            <body>
                <a href ="account.jsp">Account / </a>
                <a href ="logout.jsp">Logout</a>
                <h1>Main Page</h1>
                <h2>Here is your orders:</h2>
                               
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
    <xsl:template match="orders" >
                        
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Movie Title</th>
                                <th>Movie Genre</th>
                                <th>Movie Price</th>
                                <th>Movie Release Date</th>
                                <th>Movie Copies Purchased</th>
                                <th>User Full Name</th>
                                <th>User Email</th>
                                <th>Pay Method</th>
                                <th>Total Sale</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates />
                        </tbody>
                    </table>
        
    </xsl:template>
    <xsl:template match="order">
        <tr>
            <td>
                <xsl:value-of select="movieTitle"/>
            </td>
            <td>
                <xsl:value-of select="movieGenre"/>
            </td>
            <td>
                <xsl:value-of select="moviePrice"/>
            </td>
            <td>
                <xsl:value-of select="movieReleaseDate"/>
            </td>
            <td>
                <xsl:value-of select="movieCopiesPurchased"/>
            </td>
            <td>
                <xsl:value-of select="userFullName" />
            </td>
            <td>
                <xsl:value-of select="userEmail" />
            </td>
            <td>
                <xsl:value-of select="payMethod" />
            </td>
            <td>
                <xsl:value-of select="totalSale" />
            </td>
            <td>
                <xsl:value-of select="status" />
            </td>
            <td>
                <a href ="cancel.jsp?id={id}">Cancel</a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>