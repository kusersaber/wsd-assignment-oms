<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>      
            <head>
                <!-- <link rel="stylesheet" href="css/style.css" type="text/css"></link> -->
            </head>     
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
    <xsl:template match="movies" >
        <table border="1">
            <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Genre</th>
                    <th>Release Date</th>
                    <th>Price</th>
                    <th>Available Copies</th>
                </tr>
            </thead>
            <tbody>
                <xsl:apply-templates />
            </tbody>
        </table>
    </xsl:template>
    <xsl:template match="movie">
        <tr>
            <td>
                <img src="images/{id}.jpg" with="200" height="200"/>
            </td>
            <td>
                <xsl:value-of select="title"/>
            </td>
            <td>
                <xsl:value-of select="genre"/>
            </td>
            <td>
                <xsl:value-of select="releaseDate" />
            </td>
            <td>
                <xsl:value-of select="price" />
            </td>
            <td>
                <xsl:value-of select="availableCopies" />
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>