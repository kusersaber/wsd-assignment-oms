<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>      
            <head>
                <!-- <link rel="stylesheet" href="css/style.css" type="text/css"></link> -->
            </head>     
            <body>
                <a href ="login.jsp">Login / </a>
                <a href ="register.jsp">Register</a>
                <h1>Welcome to Online Movie Store !</h1>
                <h2>Here is our collection of movies:</h2>
                
                <p>For Search, select radio, type and search.</p>
                
                <form action="index.jsp" method="post" target="_self" >
                    <table>                
                        <tr>
                            <td>
                                <input type="radio" name="searchRadio" value="title"/> Title |
                                <input type="radio" name="searchRadio" value="genre"/> Genre >></td>
                            <td>Input: <input type="text" name="titleGenreInput"/></td>                               
                              
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="searchRadio" value="releaseDate"/> Release Date >>>>></td>  
                            <td>From: <input type="text" name="fromReleaseDate"/>
                                Until: <input type="text" name="untilReleaseDate"/>
                            </td>
                            <td>
                                <p>* From field must be older year than or equal to Until field!!</p>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <input class="button" type="submit" value="Search"/>
                            </td>
                        </tr>
                    </table>
                </form>
                               
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
    <xsl:template match="movies" >
                        
                <form action="userCheckout.jsp" method="post">
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Genre</th>
                                <th>Release Date</th>
                                <th>Price</th>
                                <th>Available Copies</th>
                                <th>Details</th>
                                <th>Buy</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates />
                        </tbody>
                    </table>
                    <br/>
                    <input type="submit" value="Checkout" />
                </form>
        
    </xsl:template>
    <xsl:template match="movie">
        <tr>
            <td>
                <xsl:value-of select="title"/>
            </td>
            <td>
                <xsl:value-of select="genre"/>
            </td>
            <td>
                <xsl:value-of select="releaseDate" />
            </td>
            <td>
                <xsl:value-of select="price" />
            </td>
            <td>
                <xsl:value-of select="availableCopies" />
            </td>
            <td>
                <a href ="movieDetails.jsp?id={id}">View</a>
            </td>
            <td>
                <!-- <a href ="checkout.jsp?id={id}">Buy</a> -->
                <input type="checkbox" name="id" value="{id}" />
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>