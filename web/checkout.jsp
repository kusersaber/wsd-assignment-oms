<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="uts.wsd.*"%>
<%@page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Checkout Page</title>
    </head>
    <% String moviesPath = application.getRealPath("WEB-INF/movies.xml");%>
    <jsp:useBean id="moviesApp" class="uts.wsd.MovieApplication" scope="application">
        <jsp:setProperty name="moviesApp" property="filePath" value="<%=moviesPath%>"/>
    </jsp:useBean>
    <%Movies movies = moviesApp.getMovies();%>

    <% String ordersPath = application.getRealPath("WEB-INF/history.xml");%>
    <jsp:useBean id="ordersApp" class="uts.wsd.OrderApplication" scope="application">
        <jsp:setProperty name="ordersApp" property="filePath" value="<%=ordersPath%>"/>
    </jsp:useBean>
    <%Orders orders = ordersApp.getOrders();%>

    <%
        String email = request.getParameter("email");
        String fullName = request.getParameter("fullName");

        String[] select = (String[]) session.getAttribute("select");
    %>

    <body>
        <h1>Checkout</h1>

        <form method="post" action="checkout.jsp">
            <%
                Random rand = new Random();
                int n = rand.nextInt(999) + 100;
               
                //String select[] = request.getParameterValues("id");
                if (select != null && select.length != 0) {
                    for (int i = 0; i < select.length; i++) {
            %>
            <ul>
                <li><%= movies.getMovie(Integer.parseInt(select[i])).getTitle()%> >>> <a href="editOrder.jsp">[Edit Order]</a></p>  
            </ul>
            <%
                        int id = n;
                        int movieId = Integer.parseInt(select[i]);
                        String movieTitle = movies.getMovie(Integer.parseInt(select[i])).getTitle();
                        String movieGenre = movies.getMovie(Integer.parseInt(select[i])).getGenre();
                        double moviePrice = movies.getMovie(Integer.parseInt(select[i])).getPrice();
                        String movieReleaseDate = movies.getMovie(Integer.parseInt(select[i])).getReleaseDate();
                        int movieCopiesPurchased = 1;
                        String userFullName = fullName;
                        String userEmail = email;
                        String payMethod = "VISA"; 
                        double totalSale = moviePrice * movieCopiesPurchased;
                        String status = "submitted";

                        if (request.getParameter("deleted") == null) {
                            Order order = new Order(id, movieId, movieTitle, movieGenre, moviePrice, movieReleaseDate, movieCopiesPurchased, userFullName, userEmail, payMethod, totalSale, status);
                                               
                            session.setAttribute("order", order);
                            orders.addOrder(order);
                            ordersApp.updateXML(orders, ordersPath);
                        }
                    }
                }
            %>
            <input type="submit" value="Delete" name="deleted">

            <a href="index.jsp">Continue Shopping...</a>
        </form>

        <%
            if (request.getParameter("deleted") != null) {

                Order order = (Order)session.getAttribute("order");
                orders.removeAll(orders.getRemoveList(order.getId()));

                ordersApp.updateXML(orders, ordersPath);
        %>
        <p>Deleted !</p>      
        <%
            }
        %>
    </body>
</html>