<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        <h1>Login</h1>
        
        <form action="loginAction.jsp" method="post">
            <table>                
                <tr><td>Email:</td><td><input type="text" name="email"></td></tr>
                <tr><td>Password:</td><td><input type="password" name="password"></td></tr>                
                <tr><td></td>
                    <td><input class="button" type="submit" value="Login">
                        <br>
                        <br>
                        You don't have an account? >>> <a href="register.jsp">Register</a>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>