<%@page import="uts.wsd.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Action Page</title>
    </head>
    <body>
        <% String userPath = application.getRealPath("WEB-INF/users.xml");%>
        <jsp:useBean id="userApp" class="uts.wsd.UserApplication" scope="application">
            <jsp:setProperty name="userApp" property="filePath" value="<%=userPath%>"/>
        </jsp:useBean>
        <% 
            Users users = userApp.getUsers();
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            User user = users.login(email, password);
            
            if (user != null) {
                session.setAttribute("userLogin", user);
                    
                    response.sendRedirect("welcomeLoginRegister.jsp"); 
                
            } else {
                session.setAttribute("existErr", "User profile does not exist!");
                response.sendRedirect("login.jsp");                               
            }
        %>
    </body>
</html>