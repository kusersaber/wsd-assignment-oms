<%@page import="uts.wsd.*" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<% String historyPath = application.getRealPath("WEB-INF/history.xml");%>
<jsp:useBean id="ordersApp" class="uts.wsd.OrderApplication" scope="application">
    <jsp:setProperty name="ordersApp" property="filePath" value="<%=historyPath%>"/>
</jsp:useBean>
<%
    Orders orders = ordersApp.getOrders();

    User user = (User) session.getAttribute("userLogin");
    String email = user.getEmail();
%>

<c:set var = "xmltext"> 
    <orders>
        <%
            for (Order order : orders.getList()) {
                if (email.equals(order.getUserEmail())) {
        %>
        <order>
            <id><%= order.getId()%></id>
            <movieId><%= order.getMovieId()%></movieId>
            <movieTitle><%= order.getMovieTitle()%></movieTitle>
            <movieGenre><%= order.getMovieGenre()%></movieGenre>
            <moviePrice><%= order.getMoviePrice()%></moviePrice>
            <movieReleaseDate><%= order.getMovieReleaseDate()%></movieReleaseDate>
            <movieCopiesPurchased><%= order.getMovieCopiesPurchased()%></movieCopiesPurchased>       
            <userFullName><%= order.getUserFullName()%></userFullName>
            <userEmail><%= order.getUserEmail()%></userEmail>
            <payMethod><%= order.getPayMethod()%></payMethod>
            <totalSale><%= order.getTotalSale()%></totalSale>
            <status><%= order.getStatus()%></status>
        </order>
        <%}%> 
        <%}%>    
    </orders>
</c:set>
<c:import url = "file:\\C:\\Users\\Louis\\Desktop\\new_folder\\wsd_assg\\build\\web\\xsl\\history.xsl" var = "xslt"/>
<x:transform xml = "${xmltext}" xslt = "${xslt}"></x:transform>