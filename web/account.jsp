<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="uts.wsd.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Account Page</title>
    </head>
    <% String userPath = application.getRealPath("WEB-INF/users.xml");%>
        <jsp:useBean id="userApp" class="uts.wsd.UserApplication" scope="application">
            <jsp:setProperty name="userApp" property="filePath" value="<%=userPath%>"/>
        </jsp:useBean>
    
    <%
        //User user = (User) session.getAttribute("user");
        User user = (User) session.getAttribute("userLogin");
    %>
    <body>
        <h1>Account</h1>
        
        <form method="post" action="account.jsp">
            <table>
                <tr><td>Email</td><td><input type="text" name="email" value="<%= user.getEmail()%>"></td></tr>
                <tr><td>Password</td><td><input type="password" name="password" value="<%= user.getPassword()%>"></td></tr>
                <tr><td>Full name</td><td><input type="text" name="fullname" value="<%= user.getFullName()%>"></td></tr>
                <tr><td>DOB</td><td><input type="text" name="dob" value="<%= user.getDob()%>"></td></tr>
                <tr><td>Phone</td><td><input type="text" name="phone" value="<%= user.getPhone()%>"></td></tr>
                <tr><td>Address</td><td><input type="text" name="address" value="<%= user.getAddress()%>"></td></tr>
                
                <tr><td><input type="hidden" value="updated" name="updated"></td>
                    <td><input type="submit" value="Save">
                        
                        <input type="submit" value="Delete" name="deleted">
                        <br>
                        <br>
                        Return to the <a href = 'main.jsp' >main page</a>.
                    </td>
            </table>
        </form> 
        <%
            if (request.getParameter("updated") != null) { 
            user.updateDetails(request.getParameter("password"), request.getParameter("fullname"), 
                    request.getParameter("dob"), request.getParameter("phone"), request.getParameter("address"));
                    userApp.editUser(user, user.getPassword(), user.getFullName(), user.getDob(), user.getPhone(), user.getAddress());
            }
        %>
        
        
        <% 
            if (request.getParameter("deleted") != null) { 
                
                Users users = userApp.getUsers();
                users.remove(user);
                userApp.updateXML(users, userPath);
                //response.sendRedirect("index.jsp");
                //session.invalidate();
            }
        %>  
    </body>
</html>
