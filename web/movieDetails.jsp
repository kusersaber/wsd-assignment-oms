<%@page contentType="application/xml"%><?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="xsl/movieDetails.xsl"?>
<%@page import="uts.wsd.*" import="java.util.*" %>

<% String filePath = application.getRealPath("WEB-INF/movies.xml");%>
<jsp:useBean id="moviesApp" class="uts.wsd.MovieApplication" scope="application">
    <jsp:setProperty name="moviesApp" property="filePath" value="<%=filePath%>"/>
</jsp:useBean>
<%
    Movies movies = moviesApp.getMovies();
    int movieId = Integer.parseInt(request.getParameter("id"));
    Movie movie = movies.getMovie(movieId);
%>
<movies>
    <movie>
        <id><%= movieId%></id>
        <title><%= movie.getTitle()%></title>
        <genre><%= movie.getGenre()%></genre>       
        <releaseDate><%= movie.getReleaseDate()%></releaseDate>
        <price><%= movie.getPrice()%></price>
        <availableCopies><%= movie.getAvailableCopies()%></availableCopies>
    </movie>   
</movies>