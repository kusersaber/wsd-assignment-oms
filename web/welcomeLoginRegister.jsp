<%@page contentType="text/html" import="java.util.*" import="uts.wsd.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>welcome Login Register Page</title>
    </head>
    <!-- For register -->
    <% String userPath = application.getRealPath("WEB-INF/users.xml");%>
        <jsp:useBean id="userApp" class="uts.wsd.UserApplication" scope="application">
            <jsp:setProperty name="userApp" property="filePath" value="<%=userPath%>"/>
        </jsp:useBean>
        <% 
            // For register
            Users users = userApp.getUsers();
        %>
    <%
        // For register
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String fullName = request.getParameter("fullName");
        String dob = request.getParameter("dob");      
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
    %>
    
    
    <%
        User loginUser = (User)session.getAttribute("userLogin");
    %>
    <body>
        <!-- If the session is a login user -->
        <% if(loginUser != null) { %>        
        
            You are logged in as <%= loginUser.getFullName() %> <%= loginUser.getEmail() %>
                <br>
                <a href="main.jsp">Main</a>
                <a href="account.jsp">My Account</a>
                <a href="logout.jsp">Logout</a> 
        
        <%            
            //session.setAttribute("user",loginUser);
            session.setAttribute("userLogin", loginUser);
        %>
        <!-- Otherwise, a registering user -->
        <%} else { %> 
        
        <%       
            try {
                    User user = new User(email, password, fullName, dob, phone, address);
                    //session.setAttribute("user",user);
                    users.addUser(user);
                    userApp.updateXML(users, userPath);
                    
                    session.setAttribute("userLogin", user);
                
            } catch (Exception e) {
                
                    session.setAttribute("existErr", "Unsuccessful register!");
                    response.sendRedirect("register.jsp");
            }
        %> 
        
            You are logged in as <%= fullName %> <%= email %>
                <br>
                <a href="main.jsp">Main</a>
                <a href="account.jsp">My Account</a>
                <a href="logout.jsp">Logout</a> 
                
        <%}%>        
    </body>
</html>
