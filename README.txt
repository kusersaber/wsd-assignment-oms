Setup:

To run search function in index.jsp, some modifications are required:

1- In package uts.controller:

*class TestXMLFactory.java change the filepath for movies.xml the current filepath of your PC:

String filePath = (new File("\\C:\\.....put the correct path here.....\WEB-INF\\movies.xml")).getAbsolutePath();


*class XMLGenerator.java change the filepath for books.xsl the current filepath on your PC:

StreamSource xslt = new StreamSource("\\C:\\....put the correct path here.....\web\\xsl\\movies.xsl");


2- In index.jsp, edit an import jstl tag and include the correct path pf movies.xsl of your PC:

   <c:import url = "file:\\D:\\NetBeans\.....put correct path here.....\xsl\\books.xsl" var = "xslt"/>


This is a data-centric XML generation in JSP.