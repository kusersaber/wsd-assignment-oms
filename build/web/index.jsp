<%@page import="uts.wsd.*" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<% String filePath = application.getRealPath("WEB-INF/movies.xml");%>
<jsp:useBean id="moviesApp" class="uts.wsd.MovieApplication" scope="application">
    <jsp:setProperty name="moviesApp" property="filePath" value="<%=filePath%>"/>
</jsp:useBean>
<%Movies movies = moviesApp.getMovies();%>
<%
    String searchRadio = request.getParameter("searchRadio");
    String titleGenreInput = request.getParameter("titleGenreInput");
    
    String fromReleaseDate = request.getParameter("fromReleaseDate");
    String untilReleaseDate = request.getParameter("untilReleaseDate");
    
        ArrayList<Movie> matches = new ArrayList<Movie>();
        if ("title".equals(searchRadio) && titleGenreInput != null && !titleGenreInput.isEmpty()) {
            matches = movies.getMatches(titleGenreInput);
        }
        else if ("genre".equals(searchRadio) && titleGenreInput != null && !titleGenreInput.isEmpty()) {
            matches = movies.getMatches(titleGenreInput);
        }
        else if ("releaseDate".equals(searchRadio) && fromReleaseDate != null && !fromReleaseDate.isEmpty() && untilReleaseDate != null && !untilReleaseDate.isEmpty()) {
            matches = movies.getYearBetween(fromReleaseDate, untilReleaseDate);
        }
        else {
            matches = movies.getMovies();
        }
%>

<c:set var = "xmltext"> 
    <movies>
    <%
        for (Movie movie : matches) {
    %>
    <movie>
        <id><%= movie.getId()%></id>
        <title><%= movie.getTitle()%></title>
        <genre><%= movie.getGenre()%></genre>       
        <releaseDate><%= movie.getReleaseDate()%></releaseDate>
        <price><%= movie.getPrice()%></price>
        <availableCopies><%= movie.getAvailableCopies()%></availableCopies>
    </movie>
    <%}%>    
    </movies>
</c:set>
<c:import url = "file:\\C:\\Users\\Louis\\Desktop\\new_folder\\wsd_assg\\build\\web\\xsl\\movies.xsl" var = "xslt"/>
<x:transform xml = "${xmltext}" xslt = "${xslt}"></x:transform>